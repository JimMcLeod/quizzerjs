# Javascript 102
I've build a small app that uses the concepts from JS101 and introduces a few more features of Javascript

We will learn about

* The global scope
* IIFEs (Immediately Invoked Function Expressions)
* Events
* Promises
* Basic DOM manipulation
* Programmatic navigation including query parameters

# Lets play a game
Have a game of the finished app

As simple as this app is it uses some of the most used features of HTML5

# The global scope
In Javascript the global scope is the web page where we are executing our code. This is initialised when we enter a new page so any variables, objects etc that we create are not carried over from page to page.

* Why don't we want to pollute the global scope?
    - we might want to use pre-built libraries and this ensures we don't get name collisions - name collisions are when a function you introduce already exists in the global scope - ie the 'window' object.
    - global objects can be modified by other scripts sharing the global space
* How do we prevent these issues?
    - use an IIFE!

# IIFEs
```
(() => {
    // code goes here
})();
```

As you can see this is a function contained in parenthesis, with another set of parenthesis at the end.

This will be called as soon as the .js file is loaded. We can pass values to the IIFE by including them in the second set of parenthesis, which can be useful for passing in results of other IIFEs or libraries we have pre-loaded.

We can have multiple IIFEs and they are executed in order.

**Give overview of IIFEs. See slides/examples/multiple-iifes.js**

**Explain the IIFE in our app. Talk about how it returns an object containing the functions we want to expose and the functions we don't expose**


# Events
There are many event types in Javascript that do different things such are react to clicks, fire when the user focuses on an element, tell us when a page has loaded. We wil just talk about a couple of these to give us a feel as to how events work.

**Explain the click event in index.html**

### Explanation of the document.addEventListener in our app
The global object 'document' holds the Document Object Model (DOM) which is a programming interface for web documents.

It represents the page so we can change the document structure, style, and content. The DOM represents the document as nodes and objects; that way, Javascript can interact with the page. It has methods we can call and properties we set to affect the page.

I'll come back to this in a little bit more detail later but one of the things we can do with the document object is set up event listeners. In this case 'DOMContentLoaded' which will fire when the, as the name suggests, when the DOM content is fully loaded and call the function specified as a parameter.

**Q** Notice the first thing that happens in this function is we check to see which page we are on before executing the inner block. Anyone think why we would do this?

**A** Commonly .js files are shared between multiple pages and we only want the inner block to be executed if we are on the 'quiz.html' page.

Notice the function we call is prefix by 'async'. This is how we tell the compiler we expect this function to ba a asynchronous function. An async function can use the 'await' keyword to elegantly handle functions that return promises.

# Promises

A promise is a placeholder for a value that will be determined at some later time or will fail and throw an exception.

Promises have three states:
* pending - the initial state. The operation is ongoing
* resolved - the operation was successful
* rejected - the operation has failed

Promises are used for operations that we don't know the result of immediately, such as a call to a backend service or transforming a large data set.

Inside an async function, we use the await keyword before a call to a function that returns a promise. This makes the code wait at that point until the promise is settled, at which point the fulfilled value of the promise is treated as a return value, or if rejected an exception is thrown that we catch.

**Explain how the promise in the app works**

**Explain ways of handling concurrent promises. See slides/examples/promises.html**

# Fetch API

**Explain how fetch is used in the app and the parameters**
# Basic DOM manipulation

Now we have our questions we want to display them. To this we must do some DOM manipulation.

First we must find the element we want to affect. There are a number of ways to address elements. Two of the most common are:
```
const element = document.getElementById('element-id');
``` 
this will return the first element with the specified ID and is one of the reasons why element IDs should be unique.
```
const elements = document.getElementsByClassName('class-name');
```
this will return an array of elements that have the class 'class-name'. This is useful when we want to iterate over a set of elements.

(an alternate way of selecting node is querySelector & querySelectorAll - the difference being querySelector & querySelectorAll don't return 'live' nodes, which means that changes in the DOM are not reflected in the result. getElementsByTagName is another that will return a 'live' node)

Now we have our element/s there are many things we can do such as adding HTML, changing/adding classes, binding events.

To add HTML to an element we use:
```
const html = '<h1>Title</h1>'

const element = document.getElementById('title-element');
element.innerHTML = html;
```
**Explain the spread operator and how it applies to arrays**

**Explain how we use a template literal to add the custom HTML in the app**

**Explain how we alter the style to hide/show buttons**

**Explain the difference between strict equality and equality operators**

# Navigation / query parameters

If we want to navigate to another page programmatically we can use the 'window' global object.
```
window.location.href = 'some url'
```

If we want to pass a value or a set of values from one page to another we use query parameters:

```
window.location.href = 'some url?param1=value1&param2=value2'
```

And to retrieve these values we can use:

```
const urlParams = new URLSearchParams(window.location.search);
const param1Value = urlParams.get('param1');
```

which would set param1Value to value1.

**Explain the use of query params in the app and how it's passed to the results.html page and how it's insecure**

# this in Javascript (redacted)

Alone, this refers to the global object.\
In a non-arrow function, this refers to the global object.\
In an object it refers to the object.\
In an event, this refers to the element that received the event.

The big gotcha here is when you call a function you might expect this to refer to the function but it doesn't, it's the global object. Unless we use an arrow function then it inherits this from it's parent.
