document.addEventListener("DOMContentLoaded", () => {
    const urlParams = new URLSearchParams(window.location.search);
    const score = urlParams.get('score');

    const resultElement = document.getElementById('result-container');
    resultElement.innerHTML = `You scored ${score} out of 10`;
});

const restartGame = () => {
    window.location.href = "index.html";
}
