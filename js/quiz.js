quizzerJs = (() => {

    let questions;
    let questionNumber;
    let answers;
    let score;
    let correctAnswerIndex;

    document.addEventListener("DOMContentLoaded", async () => {
        if (window.location.toString().includes('quiz.html')) {
            questions = await getQuestions();

             startQuiz();
        }
    });

    const getQuestions = async () => {
        try {
            const response = await fetch('https://the-trivia-api.com/api/questions?limit=10', {
                method: "GET", // *GET, POST, PUT, DELETE, etc.
                mode: "cors", // no-cors, *cors, same-origin
                cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                  "Content-Type": "application/json"
                },
                body: null
              });

            const result = response.json();
            return result;
        } catch(err) {
            // error handling goes here
        }   
    }

    // could also write this as
    // const response = await fetch('https://the-trivia-api.com/api/questions?limit=10');
    
    const startQuiz = () => {
        questionNumber = 0;
        score = 0;
        displayQuestion(questionNumber);
    }

    const displayQuestion = (questionNumber) => {
        const question = questions[questionNumber];
        answers = shuffleAnswers([ ...question.incorrectAnswers, question.correctAnswer]);

        const questionTemplate = `
            <h2 class="question">${question.question}</h2>
            <p class="answer" id="answer0" onclick="quizzerJs.selectAnswer(0)">${answers[0]}</p>
            <p class="answer" id="answer1" onclick="quizzerJs.selectAnswer(1)">${answers[1]}</p>
            <p class="answer" id="answer2" onclick="quizzerJs.selectAnswer(2)">${answers[2]}</p>
            <p class="answer" id="answer3" onclick="quizzerJs.selectAnswer(3)">${answers[3]}</p>
        `;

        const questionElement = document.getElementById('question-container');
        questionElement.innerHTML = questionTemplate;
       
        let index = 0;
        for (const answer of answers) {
            if (answer === question.correctAnswer) {
                correctAnswerIndex = index;
            }
            index++;
        }
    }

    const shuffleAnswers = (answersArray) => {
        for (let i = answersArray.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [answersArray[i], answersArray[j]] = [answersArray[j], answersArray[i]];
        }

        return answersArray;
    }

    const selectAnswer = (answer) => {
        const selectedAnswerElement = document.getElementById(`answer${answer}`);

        const correctAnswerElement = document.getElementById(`answer${correctAnswerIndex}`);
        correctAnswerElement.style.backgroundColor = 'green'; 

        if (answers[answer] === questions[questionNumber].correctAnswer) {
            selectedAnswerElement.style.backgroundColor = 'green';
            score = score + 1;
        } else {
            selectedAnswerElement.style.backgroundColor = 'red';
        }

        const nextButton = document.getElementById('next-button');
        nextButton.style.display = 'inline-block';
    }

    const nextQuestion = () => {
        const nextButtonElement = document.getElementById('next-button');
        nextButtonElement.style.display = 'none';

        questionNumber++;
        if (questionNumber === 10) {
            finishQuiz();
        } else {
            displayQuestion(questionNumber);
        }
    }

    const finishQuiz = () => {
        const resultsButtonElement = document.getElementById('results-button');
        resultsButtonElement.style.display = 'inline-block';
    }

    const showResults = () => {
        window.location.href = `results.html?score=${score}`;
    }

    const startGame = () => {
        window.location.href = "quiz.html";
    }

    return {
        nextQuestion,
        showResults,
        selectAnswer,
        startGame
    }

})();
